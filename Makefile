### Make parameters:
# VERSION=
# BINARY_NAME=
# BUILD_ID=
# BUILD_NUMBER=
# RELEASE_STRING=
# ARCH= [| cortex-m3 | linux 64]
#

### Issues
# No multi arch bulds yet
#

### Set Project options
#
ARCH ?= cortex-m3
MMCU 			:=
PROJECT_NAME	:= modular-make

ifdef DEBUG
OPTIMIZATION	:= -Og
else
OPTIMIZATION	:= -Os
endif

### Build flags for all targets
#
C_ALL_FLAGS        =
C_FLAGS            =
CXX_FLAGS          =
LINKER_FLAGS       =
LINKER_POST_FLAGS  =

GCC_COMMAND_PREFIX =

### Build tools
#
XCC             = $(GCC_COMMAND_PREFIX)gcc
XCXX            = $(GCC_COMMAND_PREFIX)gcc
XAR            	= $(GCC_COMMAND_PREFIX)ar
XRANLIB        	= $(GCC_COMMAND_PREFIX)ranlib
C_COMP          = $(XCC) $(C_FLAGS) $(C_ALL_FLAGS) $(MMCU) -o $@ -c $<
CXX_COMP        = $(XCXX) $(CXX_FLAGS) $(C_ALL_FLAGS) $(MMCU) -o $@ -c $<
LINK            = $(XCC) $(LINKER_FLAGS) $(MMCU) -o $@ $^ $(LINKER_POST_FLAGS)
AR				= $(XAR) cr $@ $^
RANLIB			= $(XRANLIB) $@
OBJCOPY			= $(GCC_COMMAND_PREFIX)objcopy
READELF			= $(GCC_COMMAND_PREFIX)readelf
MKDIR 			= mkdir -p
### Standard parts
#
include root.mk

### Main part
#
