# Modular Make

This project is a template/demo to start the build process for a new project.
It is just my prefered style of a modular build. A main Makefile includes
sub-makefiles which can again include other sub-makefiles. It is designed
for the requirements of a multi platform built.

## Getting Started

Have a look into the makefiles. The Top-level makefile sets a lot of variables
regarding the compiler, tools and general project setting. It also includes
the top-level part root.mk. This file sets the basic compiler parameters, add
set defines and basic includes. Three levels of sub makefile parts (sub.mk)
can be added to one of these lists:

* SUBDIRS_PRE
* SUBDIRS
* SUBDIRS_POST

It adds directory independent rules such as object and dependency build, clean,
directory creation.

The build process is meant to create the file structure of the code in an output
dir and put all object and dependency files there.

### Prerequisites

The build is based on a GCC environment, but it can maybe modified for other
toolchains.
Only make and a compiler is required, but some linux/unix tools like mkdir
are required as well.

### Usage

The following Variables may be append to the make call to customize the build.

* ARCH= This variable can change the included sub.mk files. This makes it possible to use the same process for different target platforms.
* BINARY_NAME=<binary-name> Sets the output filename
* VERSION=<version> Sets a version name. Will be defined and is available in code files. When no binary name is set, it witt also pe appended to the output filename.
* BUILD_ID=<build id> When no binary name is set, it witt also pe appended to the output filename.
* BUILD_NUMBER=<build number> When no binary name is set, it witt also pe appended to the output filename.
* RELEASE_STRING=<release string> Will be defined and is available in code files. When no binary name is set, it witt also pe appended to the output filename.
* DEBUG Debug build. Standard configuration switches the optimization to -Og (good for debugging)

## Authors

* **Marc Luehr** - *Initial work* - [Th3Link](https://gitlab.com/Th3Link)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
