# Standard things

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)
SAVE_SUBDIRS := $(SUBDIRS)

# Subdirectories, in random order
SUBDIRS := 
$(foreach dir,$(addprefix $(d)/,$(SUBDIRS)), $(eval include $(dir)/sub.mk))

# Local variables
CPP_SOURCES_TMP := $(wildcard $(d)/*.cpp)
CPP_SOURCES_TMP += $(wildcard $(d)/$(ARCH)/*.cpp)
C_SOURCES_TMP := $(wildcard $(d)/*.c)
C_SOURCES_TMP += $(wildcard $(d)/$(ARCH)/*.c)

OBJS_$(d)	:= $(foreach source,$(CPP_SOURCES_TMP), $(OUT_DIR)/$(source:.cpp=.o))
OBJS_$(d)	+= $(foreach source,$(C_SOURCES_TMP), $(OUT_DIR)/$(source:.c=.o))
DEPS_$(d)	:= $(OBJS_$(d):%=%.d)

DIRECTORIES := $(DIRECTORIES) $(d)/ $(d)/$(ARCH)
OBJECTS 	:= $(OBJECTS) $(OBJS_$(d))
CLEAN		:= $(CLEAN) $(OBJS_$(d)) $(DEPS_$(d))

# Local rules

C_ALL_FLAGS := $(C_ALL_FLAGS) -I"$(d)"

# Standard things

ifeq ($(filter $(MAKECMDGOALS),clean),)
-include	$(DEPS_$(d))
endif

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
SUBDIRS := $(SAVE_SUBDIRS)
