# Standard stuff

.SUFFIXES:
.SUFFIXES:	.cpp .o

all:		targets

TARGET_DIR 		:= build/$(ARCH)
OUT_DIR 		:= $(TARGET_DIR)/objects

#################################
# Collecting build information
#################################
BUILD_ID ?= $(shell date +%C%g-%m-%d_%H-%M-%S)
BUILD_NUMBER ?= $(shell whoami)
RELEASE_STRING ?= $(PROJECT_NAME)-$(BUILD_NUMBER)-$(BUILD_ID)
VERSION ?= NO_VERSION

#################################
# Defines
#################################
DEFINES 	+= -DRELEASE_STRING="\"$(RELEASE_STRING)\""

# Subdirectories, in order
SUBDIRS_PRE := arch-config
$(foreach dir,$(SUBDIRS_PRE), $(eval include $(dir)/sub.mk))

# Subdirectories, in random order
SUBDIRS := main component1 component2
$(foreach dir,$(SUBDIRS), $(eval include $(dir)/sub.mk))

# Subdirectories, in order
SUBDIRS_POST := build
$(foreach dir,$(SUBDIRS_POST), $(eval include $(dir)/sub.mk))

CLEAN			:= $(CLEAN) $(OUT_DIR)

C_ALL_FLAGS 	:= $(C_ALL_FLAGS) -I.
LINKER_FLAGS   	:= -Wall $(OPTIMIZATION) $(LINKER_FLAGS)
C_FLAGS 		:= $(C_FLAGS) -std=gnu99 -Wall $(OPTIMIZATION)
CXX_FLAGS 		:= $(CXX_FLAGS) -std=c++11 -Wall $(OPTIMIZATION)

REQUIREMENTS := $(REQUIREMENTS) directories

CLEAN		:= $(CLEAN)

# General directory independent rules

$(OUT_DIR)/%.o:	%.cpp | $(REQUIREMENTS)
	$(CXX_COMP) -DBUILDNUMBER="\"$(BUILD_NUMBER)\""

$(OUT_DIR)/%.o:	%.c | $(REQUIREMENTS)
	$(C_COMP) -DBUILDNUMBER="\"$(BUILD_NUMBER)\""
	
$(OUT_DIR)/%.o.d:	%.c | $(REQUIREMENTS)
	$(C_COMP) -MM -MT $(@:.d=)

$(OUT_DIR)/%.o.d:	%.cpp | $(REQUIREMENTS)
	$(CXX_COMP) -MM -MT $(@:.d=)

%:		%.o
	$(LINK)

.PHONY:		targets
targets:	$(REQUIREMENTS) $(TARGETS) $(EXTRAS) $(LIBRARIES)

.PHONY:		clean
clean:		$(CLEAN_RULES)
	rm -Rf $(CLEAN)


DIRECTORIES_TMP := $(addprefix $(OUT_DIR)/,$(DIRECTORIES))
$(DIRECTORIES_TMP):
	$(MKDIR) $@
directories: $(DIRECTORIES_TMP)
	
# Prevent make from removing any build targets, including intermediate ones

.SECONDARY:	$(CLEAN)
