# Standard things
sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)
SAVE_SUBDIRS := $(SUBDIRS)

# Subdirectories, in random order
SUBDIRS := 
$(foreach dir,$(addprefix $(d)/,$(SUBDIRS)), $(eval include $(dir)/sub.mk))

# Standard things
d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
SUBDIRS := $(SAVE_SUBDIRS)
