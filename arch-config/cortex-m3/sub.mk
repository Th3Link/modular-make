# Standard things
sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)
SAVE_SUBDIRS := $(SUBDIRS)

# Subdirectories, in random order
SUBDIRS := 
$(foreach dir,$(addprefix $(d)/,$(SUBDIRS)), $(eval include $(dir)/sub.mk))

MMCU 			:= -mcpu=$(ARCH) -mthumb
LINKER_FLAGS  	:= $(LINKER_FLAGS) --specs=nosys.specs
GCC_COMMAND_PREFIX := arm-none-eabi-

# Standard things
d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
SUBDIRS := $(SAVE_SUBDIRS)
