# Standard things

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)
SAVE_SUBDIRS := $(SUBDIRS)

# Subdirectories, in random order
SUBDIRS := $(ARCH)
$(foreach dir,$(addprefix $(d)/,$(SUBDIRS)), $(eval include $(dir)/sub.mk))

# Local variables
CPP_SOURCES_TMP := $(wildcard $(d)/source/*.cpp)
CPP_SOURCES_TMP += $(wildcard $(d)/source/$(ARCH)/*.cpp)
C_SOURCES_TMP := $(wildcard $(d)/source/*.c)
C_SOURCES_TMP += $(wildcard $(d)/source/$(ARCH)/*.c)

OBJS_$(d)	:= $(foreach source,$(CPP_SOURCES_TMP), $(OUT_DIR)/$(source:.cpp=.o))
OBJS_$(d)	+= $(foreach source,$(C_SOURCES_TMP), $(OUT_DIR)/$(source:.c=.o))
DEPS_$(d)	:= $(OBJS_$(d):%=%.d)

DIRECTORIES := $(DIRECTORIES) $(d)/source/ $(d)/source/$(ARCH)
OBJECTS 	:= $(OBJECTS) $(OBJS_$(d))
CLEAN		:= $(CLEAN) $(OBJS_$(d)) $(DEPS_$(d))

# Local rules

# Standard things

ifeq ($(filter $(MAKECMDGOALS),clean),)
-include	$(DEPS_$(d))
endif

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
SUBDIRS := $(SAVE_SUBDIRS)
