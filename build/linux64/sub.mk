# Standard things

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)
SAVE_SUBDIRS := $(SUBDIRS)

# Subdirectories, in random order
SUBDIRS := 
$(foreach dir,$(addprefix $(d)/,$(SUBDIRS)), $(eval include $(dir)/sub.mk))

# Local variables

OBJS_$(d)	:= 
DEPS_$(d)	:= $(OBJS_$(d):%=%.d)

DIRECTORIES := $(DIRECTORIES)
OBJECTS 	:= $(OBJECTS) $(OBJS_$(d))
CLEAN		:= $(CLEAN) $(OBJS_$(d)) $(DEPS_$(d))

CLEAN		:= $(CLEAN) $(TARGET_DIR)/*.bin $(TARGET_DIR)/*.elf $(TARGET_DIR)/*.map

##########################
# Output file informations
##########################
null      :=
SPACE     := $(null) $(null)
BINARY_NAME ?= $(subst $(SPACE),,$(PROJECT_NAME)-$(VERSION))

ELFFILE 		:= $(TARGET_DIR)/$(BINARY_NAME)
MAPFILE 		:= $(TARGET_DIR)/$(BINARY_NAME).map

TARGETS			:= $(TARGETS) $(ELFFILE)
CLEAN			:= $(CLEAN) $(ELFFILE) $(MAPFILE)
LINKER_FLAGS  	:= $(LINKER_FLAGS) -Wl,-Map=$(MAPFILE)

#################################
# Defines
#################################

# Local rules

#########################################
# Create all bin files over one rule
# elffile must be the first prerequirery
# will be referenced by $<
#########################################
	
$(ELFFILE): $(OBJECTS)
	#######################################
	# Building *.elf
	#######################################
	$(LINK)

# Standard things

ifeq ($(filter $(MAKECMDGOALS),clean),)
-include	$(DEPS_$(d))
endif

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
SUBDIRS := $(SAVE_SUBDIRS)
