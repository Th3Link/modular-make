# Standard things

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)
SAVE_SUBDIRS := $(SUBDIRS)

# Subdirectories, in random order
SUBDIRS := 
$(foreach dir,$(addprefix $(d)/,$(SUBDIRS)), $(eval include $(dir)/sub.mk))

# Local variables

OBJS_$(d)	:= 
DEPS_$(d)	:= $(OBJS_$(d):%=%.d)

DIRECTORIES := $(DIRECTORIES)
OBJECTS 	:= $(OBJECTS) $(OBJS_$(d))
CLEAN		:= $(CLEAN) $(OBJS_$(d)) $(DEPS_$(d))

CLEAN		:= $(CLEAN) $(TARGET_DIR)/*.bin $(TARGET_DIR)/*.elf $(TARGET_DIR)/*.map

##########################
# Output file informations
##########################
null      :=
SPACE     := $(null) $(null)
BINARY_NAME ?= $(subst $(SPACE),,$(PROJECT_NAME)-$(VERSION))

ELFFILE 		:= $(TARGET_DIR)/$(BINARY_NAME).elf
BINFILE 		:= $(TARGET_DIR)/$(BINARY_NAME).bin
MAPFILE 		:= $(TARGET_DIR)/$(BINARY_NAME).map
S37FILE 		:= $(TARGET_DIR)/$(BINARY_NAME).s37

TARGETS			:= $(TARGETS) $(BINFILE) $(S37FILE)
CLEAN			:= $(CLEAN) $(BINFILE) $(S37FILE)
LINKER_FLAGS 	:= $(LINKER_FLAGS) -Wl,-Map=$(MAPFILE)

#################################
# Defines
#################################

# Local rules

#########################################
# Create all bin files over one rule
# elffile must be the first prerequirery
# will be referenced by $<
#########################################
$(BINFILE): $(ELFFILE)
	#######################################
	# Building *.bin
	#######################################
	$(OBJCOPY) -O binary $< $@

$(S37FILE): $(ELFFILE)
	#######################################
	# Building *.s37
	#######################################
	$(OBJCOPY) -O srec $< $@
	
$(ELFFILE): $(OBJECTS)
	#######################################
	# Building *.elf
	#######################################
	$(LINK)

.PHONY: flash
flash: all
	#######################################
	# Downloading to target
	#######################################
	# $(BINFILE)

# Standard things

ifeq ($(filter $(MAKECMDGOALS),clean),)
-include	$(DEPS_$(d))
endif

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
SUBDIRS := $(SAVE_SUBDIRS)
